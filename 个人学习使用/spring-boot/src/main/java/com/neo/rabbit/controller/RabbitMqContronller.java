package com.neo.rabbit.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.neo.model.Result;
import com.neo.rabbit.producer.Sender;
import com.neo.util.ResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * rabbitmq测试contronller
 * @author wang
 * @date 2018年4月3日
 */
@RestController
@Api(value="rabbitMqContronller",description="rabbitmq测试类")
public class RabbitMqContronller {

    public static final Logger logger  = LoggerFactory.getLogger(RabbitMqContronller.class);

    @Autowired
    private Sender sender ;

    @ApiOperation(value = "简单模式发送String消息",notes = "简单模式发送String消息" )
    @ApiImplicitParams({
            @ApiImplicitParam(name="Accept",value="接收属性",required=true,dataType="String",paramType="header",defaultValue="application/json"),
            @ApiImplicitParam(name="Accept-Charset",value="接受字符集",required=true,dataType="String",paramType="header",defaultValue="utf-8"),
            @ApiImplicitParam(name="Content-Type",value="内容类型",required=false,dataType="String",paramType="header",defaultValue="application/json")
    })
    @GetMapping("/rabbitmq/directsend/a")
    public Result<?> sendStringMq(@RequestParam String message, @RequestHeader HttpHeaders headers){   //这里用于测试，key值可以自定义实现

        try {
            sender.send(message);
            return ResultUtils.success(message);
        } catch (Exception e) {
            // TODO: handle exception
            logger.error("mq发送消息失败:"+e.getMessage(), e);
        }

        return ResultUtils.error(1, "mq发送消息失败");

    }

    @ApiOperation(value = "简单模式发送Map消息",notes = "简单模式发送Map消息" )
    @ApiImplicitParams({
            @ApiImplicitParam(name="Accept",value="接收属性",required=true,dataType="String",paramType="header",defaultValue="application/json"),
            @ApiImplicitParam(name="Accept-Charset",value="接受字符集",required=true,dataType="String",paramType="header",defaultValue="utf-8"),
            @ApiImplicitParam(name="Content-Type",value="内容类型",required=false,dataType="String",paramType="header",defaultValue="application/json")
    })
    @GetMapping("/rabbitmq/directsend/b")
    public Result<?> sendMapMq( @RequestParam String message, @RequestHeader HttpHeaders headers){   //这里用于测试，key值可以自定义实现

        try {
            Map<String,String> map = new HashMap<>();
            map.put("name", "蚂蚁上树");
            map.put("age", "20");
            sender.sendMapMessage(map);
            return ResultUtils.success(map);
        } catch (Exception e) {
            // TODO: handle exception
            logger.error("mq发送消息失败:"+e.getMessage(), e);
        }

        return ResultUtils.error(1, "mq发送消息失败");

    }

    @ApiOperation(value = "发布/订阅模式发送消息",notes = "发布/订阅模式发送消息" )
    @ApiImplicitParams({
            @ApiImplicitParam(name="Accept",value="接收属性",required=true,dataType="String",paramType="header",defaultValue="application/json"),
            @ApiImplicitParam(name="Accept-Charset",value="接受字符集",required=true,dataType="String",paramType="header",defaultValue="utf-8"),
            @ApiImplicitParam(name="Content-Type",value="内容类型",required=false,dataType="String",paramType="header",defaultValue="application/json")
    })
    @GetMapping("/rabbitmq/pssend")
    public Result<?> sendPsMq( @RequestParam String message, @RequestHeader HttpHeaders headers){   //这里用于测试，key值可以自定义实现

        try {

            sender.psSend(message);
            return ResultUtils.success(message);
        } catch (Exception e) {
            // TODO: handle exception
            logger.error(" 发布/订阅模式下发送mq消息失败:"+e.getMessage(), e);
        }

        return ResultUtils.error(101, "发布/订阅模式下发送mq消息失败");

    }


    /*
     * 路由模式发送mq
     */
    @ApiOperation(value = "路由模式发送消息",notes = "路由模式发送消息" )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "message",value="要传送的消息jsonArray",required = true,dataType = "String" , paramType ="body"),
            @ApiImplicitParam(name="Accept",value="接收属性",required=true,dataType="String",paramType="header",defaultValue="application/json"),
            @ApiImplicitParam(name="Accept-Charset",value="接受字符集",required=true,dataType="String",paramType="header",defaultValue="utf-8"),
            @ApiImplicitParam(name="Content-Type",value="内容类型",required=false,dataType="String",paramType="header",defaultValue="application/json")
    })
    @PostMapping(value = "/rabbitmq/routingsend" ,produces ="application/json;charset=UTF-8")
    public Result<?> sendRoutingMq( @RequestBody String message, @RequestHeader HttpHeaders headers){   //这里用于测试，key值可以自定义实现

        JSONArray jsonArray = JSON.parseArray(message);
        if(jsonArray == null || jsonArray.size() <= 0) {
            return ResultUtils.error(102, "请输入有效message参数");
        }

        try {
            jsonArray.forEach(( json ) -> sender.routingSend("order",JSONObject.toJSONString(json)));
            return ResultUtils.success(message);
        } catch (Exception e) {
            // TODO: handle exception
            logger.error(" 路由模式下发送mq消息失败:"+e.getMessage(), e);
        }

        return ResultUtils.error(101, "路由模式下发送mq消息失败");

    }

    /*
     * 主题模式发送mq
     */
    @ApiOperation(value = "主题模式发送消息",notes = "主题模式发送消息" )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "message",value="要传送的消息jsonArray",required = true,dataType = "String" , paramType ="body"),
            @ApiImplicitParam(name="Accept",value="接收属性",required=true,dataType="String",paramType="header",defaultValue="application/json"),
            @ApiImplicitParam(name="Accept-Charset",value="接受字符集",required=true,dataType="String",paramType="header",defaultValue="utf-8"),
            @ApiImplicitParam(name="Content-Type",value="内容类型",required=false,dataType="String",paramType="header",defaultValue="application/json")
    })
    @PostMapping(value = "/rabbitmq/topicsend" ,produces ="application/json;charset=UTF-8")
    public Result<?> sendTopicMq( @RequestBody String message, @RequestHeader HttpHeaders headers){   //这里用于测试，key值可以自定义实现

        JSONArray jsonArray = JSON.parseArray(message);
        if(jsonArray == null || jsonArray.size() <= 0) {
            return ResultUtils.error(102, "请输入有效message参数");
        }

        try {
            jsonArray.forEach(( json ) -> sender.topicSend("user.add",JSONObject.toJSONString(json)));
            return ResultUtils.success(message);
        } catch (Exception e) {
            // TODO: handle exception
            logger.error(" 主题模式下发送mq消息失败:"+e.getMessage(), e);
        }

        return ResultUtils.error(101, "主题模式下发送mq消息失败");

    }



}

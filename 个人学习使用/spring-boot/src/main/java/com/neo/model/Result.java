package com.neo.model;

public class Result<T> {

    private String  id;
    private T  msg;

    public Result(String id, T msg) {
        this.id = id;
        this.msg = msg;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Result result = (Result) o;

        if (id != null ? !id.equals(result.id) : result.id != null) return false;
        return msg != null ? msg.equals(result.msg) : result.msg == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (msg != null ? msg.hashCode() : 0);
        return result;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public T getMsg() {
        return msg;
    }

    public void setMsg(T  msg) {
        this.msg = msg;
    }
}

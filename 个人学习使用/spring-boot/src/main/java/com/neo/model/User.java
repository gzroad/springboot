package com.neo.model;

import java.io.Serializable;

public class User implements Serializable{
    private static final long serialVersionUID = -3946734305303957850L;
    private String id;
    private String name;
    private String age;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
}

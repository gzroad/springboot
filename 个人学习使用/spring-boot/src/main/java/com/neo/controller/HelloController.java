package com.neo.controller;


import com.neo.model.User;
import com.neo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class HelloController {

    @Autowired
    private UserService userService;

    @GetMapping("/adduser")
    public int addUser(@RequestParam("name") String name, @RequestParam("age") String age) {
        return userService.addUser(name, age);
    }

    @GetMapping("/findUser")
    public User findUser(@RequestParam("id") String id) {
        return userService.findById(id);
    }

    @GetMapping("/updataById")
    public String updataById(@RequestParam("id") String id, @RequestParam("name") String name) {
        try {
            userService.updataById(id, name);
        } catch (Exception e) {
            return "error";
        }
        return "success";
    }

    @DeleteMapping("/deleteById")
    public String deleteById(@RequestParam("id") String id) {
        try {
            userService.deleteById(id);
        } catch (Exception e) {
            return "error";
        }
        return "success";
    }
}

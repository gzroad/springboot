package com.neo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableCaching
@ComponentScan(basePackages = {"com.neo"})
@MapperScan("com.neo.mapper")
public class ApplicationMq {

	public static void main(String[] args) {
		SpringApplication.run(ApplicationMq.class, args);
	}
}

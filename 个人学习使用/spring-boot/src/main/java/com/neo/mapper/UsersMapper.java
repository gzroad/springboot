package com.neo.mapper;


import com.neo.model.User;
import org.apache.ibatis.annotations.*;


@Mapper
public interface UsersMapper {

    @Insert("insert into user(name,age) values(#{name},#{age})")
    int addUser(@Param("name") String name, @Param("age") String age);

    @Select("select * from user where id =#{id}")
    User findById(@Param("id") String id);

    @Update("update user set name=#{name} where id=#{id}")
    void updateById(@Param("id") String id, @Param("name") String name);

    @Delete("delete from user where id=#{id}")
    void deleteById(@Param("id") String id);

}

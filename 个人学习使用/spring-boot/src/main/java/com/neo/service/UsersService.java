package com.neo.service;


import com.neo.mapper.UsersMapper;
import com.neo.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UsersService {

    @Autowired
    private UsersMapper usersMapper;


    public User findById(String id) {
        return usersMapper.findById(id);
    }

    public int addUser(String name, String age) {
        return usersMapper.addUser(name, age);
    }

    public void updataById(String id, String name) {
        usersMapper.updateById(id, name);
    }

    public void deleteById(String id) {
        usersMapper.deleteById(id);
    }
}

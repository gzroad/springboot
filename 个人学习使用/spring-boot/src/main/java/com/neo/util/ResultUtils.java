package com.neo.util;

import com.neo.model.Result;

public class ResultUtils {
    public static Result<?> success(Object message) {
        return new Result<Object>("",message);
    }

    public static Result<?> error(int i, String message) {
        return new Result<Object>(""+i,message);
    }
}

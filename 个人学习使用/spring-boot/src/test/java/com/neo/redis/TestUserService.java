package com.neo.redis;

import com.neo.mapper.UsersMapper;
import com.neo.model.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;
@SpringBootTest
@RunWith(SpringRunner.class)
@Component
public class TestUserService {


    /**
     *
     redisTemplate.opsForValue();//操作字符串
     redisTemplate.opsForHash();//操作hash
     redisTemplate.opsForList();//操作list
     redisTemplate.opsForSet();//操作set
     redisTemplate.opsForZSet();//操作有序set

     */
    private Log log = LogFactory.getLog(TestUserService.class);
    @Resource
    private RedisTemplate<String, User> redisTemplate;

    @Autowired
    private UsersMapper usersMapper;
    @Test
    public void testFindOne() {
        findOne("2");
    }

    @Test
    public void testUpdateUser() {
       User user =  new User();
       user.setId("2");
       user.setName("guoz");
        updateUser(user);
    }


    /**
     * 通过id查询，如果查询到则进行缓存
     * @param id 实体类id
     * @return 查询到的实现类
     */

    public User findOne(String id) {
        String key = RedisKeyPrefix.USER + id;
        // 缓存存在
        boolean hasKey = redisTemplate.hasKey(key);
        if (hasKey) { // 从缓存中取
            User girl = redisTemplate.opsForValue().get(key);
            log.info("从缓存中获取了用户！");
            return girl;
        }
        // 从数据库取，并存回缓存
        User user = usersMapper.findById(id);
        // 放入缓存，并设置缓存时间
        redisTemplate.opsForValue().set(key, user, 2000, TimeUnit.SECONDS);
        return user;
    }


    /**
     * 更新用户
     * 如果缓存存在，删除
     * 如果缓存不存在，不操作
     *
     * @param user 用户
     */
    public void updateUser(User user) {
        log.info("更新用户start...");
        usersMapper.updateById(user.getId(),user.getName());
        String userId = user.getId();
        // 缓存存在，删除缓存
        String key = RedisKeyPrefix.USER + userId;
        boolean hasKey = redisTemplate.hasKey(key);
        if (hasKey) {
            redisTemplate.delete(key);
            log.info("更新用户时候，从缓存中删除用户 >> " + userId);
        }
    }

    /**
     * 删除用户
     * 如果缓存中存在，删除
     */
    public void deleteById(String id) {
        log.info("删除用户start...");
        usersMapper.deleteById(id);

        // 缓存存在，删除缓存
        String key = RedisKeyPrefix.USER + id;
        boolean hasKey = redisTemplate.hasKey(key);
        if (hasKey) {
            redisTemplate.delete(key);
            log.info("删除用户时候，从缓存中删除用户 >> " + id);
        }
    }
}
